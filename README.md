# JPEG codec in Rust

This is an experiment in writing a new JPEG codec in Rust with the
following features:

* Uses the SIMD functions from libjpeg-turbo.
* Has easy "encode a JPEG" and "decode a JPEG" APIs.
* Has detailed APIs to build custom encoding and decoding pipelines.

Eventually the idea is to wrap the Rust API in a C API that can be
used from language bindings, to hopefully provide a modern,
general-purpose JPEG library to the world.

# Grand plan

The overall goal is to have a fast, production-quality JPEG codec with
a modern API, written in a safe language.

* **Fast:** We use libjpeg-turbo's SIMD functions pretty much
  unchanged.  These handle the inner loops for color conversion,
  Discrete Cosine Transform, resampling, etc.  [See below](#SIMD) for
  how this is achieved.

* **Production-quality:** Image formats are tricky beasts, and JPEG
  allows for many variations that are hard to capture in codecs
  written from scratch.  This codec is not a port of libjpeg-turbo to
  Rust, but it tries to preseve all the embodied knowledge from its
  code.  See the [porting strategy](#porting_strategy) below for more
  information.

* **Modern API:** Rust's type system should let us expose all levels
  of the JPEG pipeline with an API that is impossible to misuse.
  Also, we want to provide high-level APIs for common operations like
  decoding a JPEG, or just examining its headers, or rotating one by
  90 degrees losslessly.  See below for [problems with
  libjpeg-turbo](#problems_with_libjpeg_turbo) for things we want to
  avoid.

# Desiderata

Use the Rust type system to define an API that is "impossible to use
incorrectly".  Typestate patterns, early validation of things,
predictable error handling.

Wrap the libjpeg-turbo SIMD code in safe wrappers, and build the codec
on top of that.

Port the inner loops in C (the SIMD-less code) to safe Rust, and keep
them as a reference implementation / safe-only codebase.

Eventually: parallelize the parts of the encoding/decoding pipeline
that actually are embarrassingly parallel (colorspace conversion,
DCT...).

Expose a simple API in C that applications can use.  It doesn't need
to cover all the intermediate stages of the pipeline; callers with
more exotic needs than "read a JPEG" or "write a JPEG" can implement
that in Rust and expose a high level C API.

Use the API in C from GNOME / gdk-pixbuf, and remove the use of
libjpeg-turbo from GNOME.

# Problems with libjpeg-turbo

The C API in libjpeg-turbo is notoriously hard to use and extend.  It
depends on large structs with many publically-visible fields
(`jpeg_compress_struct` and `jpeg_decompress_struct`) , which combine
the user's configuration for the codec's parameters, the codec's
internal state, and private data.  The need for maintaining API and
ABI compatibility at all costs has made it very hard to change
this API.

The C code has numerous checks to ensure that the API is called in the
proper order.  The required ordering is not visible in the public API;
one has to read the documentation carefully or the code.

Most importantly, the error handling strategy is extremely problematic
for applications and language bindings.  Whenever libjpeg-turbo
encounters an error, such as a corrupted JPEG, it calls a callback
supplied by the user.  *The error callback is expected to **not
return control flow** to the library.* In effect this means that C code
is forced to `setjmp()` before calls to libjpeg-turbo, and `longjmp()`
back out from the error callback.  This is fragile and error-prone,
and gives real trouble to non-C languages that do not support
unwinding across FFI boundaries.

There is a lot of C code to handle concerns that are no longer current
for modern, fast computers with plenty of RAM.  Libjpeg-turbo can deal
with very low memory systems, has vestigial code to deal with
temporary data on disk, can do integer operations on platforms where
floating point was slow, etc.  It is fine to keep using libjpeg-turbo
on those platforms where it already works.

Libjpeg-turbo supports being compiled to handle JPEGs with 12 bits per
color component, but this creates a library that is ABI-incompatible
with the normal one for 8 bit color components.  This leads, for
example, to Linux distributions not shipping support for 12-bit JPEGs
by default.

As is customary for old C code, libjpeg-turbo has no tests for the
individual algorithms or unit tests.  This makes it hard to refactor
without breaking it.

That said, libjpeg-turbo is a mature, battle-tested, invaluable source
of real-world knowledge of how JPEG is used.  We want to preserve that
knowledge in this new codec, remove the historical/obsolete parts, and
include modern amenities like unit tests, easy-to-run benchmarks, and a
fuzz-testing framework.

# Reusing code from libjpeg-turbo

Saving what we love, instead of destroying what we hate.

## SIMD

Libjpeg-turbo has hand-written assembly code (either raw assembly or
via compiler intrinsics) for the various "little" algorithms in the
JPEG pipeline — color conversion between RGB and YCbCr, the Discrete
Cosine Transform, etc.  With a few changes, this code is easy to call
from Rust.

Internally, libjpeg-turbo makes the SIMD code callable as normal C
functions.  The code has run-time detection of the CPU and selects
architecture-specific implementations of each function — for example,
there is color conversion for x86_64 with SSE2 or AVX2 instructions.

However, some of these functions take as arguments the full
`jpeg_compress_struct` or `jpeg_decompress_struct`, or other large
structs that represent the configuration for handling color
components.  The following branch makes it so that the SIMD functions
only require small structs to be passed to them with just the data
they need, instead of all the state from the libjpeg-turbo API:

https://github.com/federicomenaquintero/libjpeg-turbo/commits/no_cinfo_in_simd

This Rust codec does not use that C and assembly code directly.
Instead, it uses a fork of mozjpeg-sys (a Rust crate to expose the
libjpeg-turbo API to Rust) that has been modified to point to that
branch, and to expose the SIMD internals:

https://github.com/federicomenaquintero/mozjpeg-sys

One benefit of mozjpeg-sys is that it replaces libjpeg-turbo's CMake
build system with a bunch of `build.rs` magic, so consumers of
mozjpeg-sys can just view it as a regular Rust crate.

A long-term project could be to rewrite the SIMD code in something
friendlier, at least using compiler intrinsics and Rust-level support
for SIMD aligned types.  This is not a high priority; we can use
libjpeg-turbo's SIMD functions as black boxes for a good while.

## Porting strategy

In contrast with the strategy that was used to port librsvg to Rust,
this codec is not a line-by-line port of libjpeg-turbo.  Doing the
former works fine, but it takes a lot of subsequent refactoring to
approach a good code structure for Rust.

Instead, we will use the knowledge of the more-or-less fixed JPEG
pipeline to have a good structure from the beginning, and preserve all
the embodied knowledge in libjpeg-turbo for the code that does the
actual work of detecting corrupt data and dealing with JPEG's special
cases.

This is the general strategy:

* Isolate the "reference implementation" in C of each stage of the
  JPEG pipeline, or each algorithm (YCbCr conversion, DCT /
  quantization, etc.).  Make it callable from Rust.

* Write a characterization test for that implementation: feed it some
  data, store what it produces, and use it as the expected result in a
  test.  Alternatively, call the C and Rust versions of the code and
  test that their outputs are the same.

* Port the reference implementation from C to Rust.  Leave this as the
  "safe code only" codepath.  Test it against the expected result.  The
  Rust code can be refactored at leisure once the tests are in place.

* Plug in the assembly version of the function; see if it produces the
  same result (lossless algorithms), or as in the case of the DCT,
  decide if it even makes sense to compare results.

* Plug these into the benchmarking infrastructure.  The code can be
  optimized at leisure after that.
