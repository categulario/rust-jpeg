// Sketches for the encoder state; not intended to compile.
//
// Note - http://cliffle.com/blog/rust-typestate/ is a good reference
// to avoid a separate struct per state.

struct Encoder {
    density: Density,
    width: u16,
    height: u16,
}

impl Encoder {
    pub fn new(width: u16, height: u16) -> Encoder {
        assert!(width > 0 && height > 0);

        Encoder {
            width,
            height,
            density: Density::default(),
        }
    }

    pub fn density(self, density: Density) -> Encoder {
        self.density = density;
        self
    }

    pub fn header_writer(self) -> HeaderWriter {
    }

    pub fn scanline_writer(self) -> Result<ScanlineWriter> {
        let headers = self.header_writer();

        headers.write()?;
        headers.scanline_writer()
    }
}

// encoder state
//
// Comes from jpegint.h - CSTATE_*
enum State {
    Start,       // after create_compress
    Scanning,    // start_compress done, write_scanlines OK
    RawOk,       // start_compress done, write_raw_data OK
    WriteCoeffs, // jpeg_write_coefficients done
}

struct Driver {
    comment: String,
    icc: Option<ICCProfile>,
    markers: FIXME,
}

// FIXME: this only supports a DCT lossy pipeline, not lossless/predictive operation mode.
struct Pipeline {
    // FIXME: where does subsampling go?
    blocks: Blocks,
    dct: FDCT,
    quantizer: Quantizer,
    zigzag: Zigzag,
    rle: RLE,
    entropy: Entropy,
}

trait Blocks {
    // Note that these are not necessarily RGB blocks, but converted to YCbCr or whatever.
    fn get_block(x: usize, y: usize) -> Block;
}

trait FDCT {
    fn dct(block: Block) -> DCTBlock;
}

trait Quantizer {
    fn quantize(dct: DCTBlock, table: QuantTable) -> QuantizedBlock;
}

trait Zigzag {
    fn zigzag(quant: QuantizedBlock) -> ZigzagBlock;
}

trait Entropy {
    fn entropy(zigzag: ZigzagBlock) -> EntropyCoded;
}

struct Huffman {
    /// Table for the first (DC) coefficient of each DCT block.
    table_dc: HuffmanTable,

    /// Table for the other 63 (AC) coefficients of each DCT block.
    table_ac: HuffmanTable,
}

impl Entropy for Huffman {}

struct Arithmetic {}

impl Entropy for Arithmetic {}

struct DCTBlock {}
