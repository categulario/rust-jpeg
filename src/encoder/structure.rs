//! Writers for the structure of a JPEG/JFIF stream

use byteorder::{BigEndian, WriteBytesExt};
use cast::{u16, u8};

use std::io::{self, Write};
use std::marker::PhantomData;
use std::mem;

use crate::common::constants::{DCT_SIZE_SQUARED, NUM_QUANT_TABLES};
use crate::common::density::{Density, Units};
use crate::common::marker::{Marker, MarkerType};
use crate::common::quant_table::QuantTable;
use crate::common::types::{Sample, Sample8};

/// Start state for JPEG encoding.
///
/// This just writes the Start Of Image (SOI) marker at the beginning
/// of a JPEG file.  With its methods you can choose whether to write a
/// JFIF/baseline image, or something more exotic.
pub struct StartOfImage;

/// State for the App0 JFIF marker.
///
/// This identifies the JPEG file as conforming to the JFIF standard (8-bit samples, YCbCr
/// colorspace) and encodes the image's density.
pub struct Jfif;

pub struct JfifExtension;

pub struct Frame<T: Sample + Sized> {
    sample: PhantomData<T>,
}

pub struct NextStateFIXME;

impl StartOfImage {
    /// Starts a JFIF/baseline encoder by writing the Start Of Image (SOI) marker.
    ///
    /// The next state the [`Jfif`] state.
    ///
    /// [`Jfif`]: struct.Jfif.html
    pub fn start_jfif<W: Write>(&self, w: &mut W) -> io::Result<Jfif> {
        write_marker(w, &Marker::new(MarkerType::StartOfImage, &[]))?;
        Ok(Jfif)
    }
}

// Returns (major, minor)
fn get_jfif_version() -> (u8, u8) {
    // FIXME: libjpeg's encoder uses version 1.1 by default; says that
    // 1.2 should be used only if the app plans to write 1.2 extension
    // markers (thumbnails).
    //
    // However, e.g. the GIMP does not set the version number even if
    // it writes a thumbnail.  (It seems to write the file with
    // libjpeg, then use exiv2 to write the thumbnail and EXIF data -
    // but that doesn't change the JFIF version, either.)
    //
    // A code search with
    // https://codesearch.debian.net/search?q=JFIF_minor_version+%3D+2&literal=0
    // indicates that at least gdal, mplayer, mpv, exactimage use 1.02.
    //
    // Should we make the decision to use 1.2 part of the "with thumbnails"
    // typestate?

    (1, 1)
}

impl Jfif {
    /// Writes the App0 JFIF marker with the image's density, and
    /// proceeds to the [`JfifExtension`] state.
    ///
    /// [`JfifExtension`]: struct.JfifExtension.html

    // Rec. ITU-T T.871 (05/2011)
    // 10.1 JFIF file syntax
    pub fn write<W: Write>(&self, density: &Density, w: &mut W) -> io::Result<JfifExtension> {
        // FIXME: see jpeg_set_colorspace(): note that the JFIF marker
        // (this typestate!) is not written if the user specifies a
        // colorspace that is not grayscale or YCbCr.

        let units = match density.units {
            Units::AspectRatio => 0,
            Units::PointsPerIn => 1,
            Units::PointsPerCm => 2,
        };

        let (major, minor) = get_jfif_version();

        #[rustfmt::skip]
        let payload = [
            // nul-terminated "JFIF"
            'J' as u8,
            'F' as u8,
            'I' as u8,
            'F' as u8,
            '\0' as u8,

            // JFIF version
            major,
            minor,

            // Density units
            units,

            // horizontal pixel density
            (density.h >> 8) as u8,
            (density.h & 0xff) as u8,

            // vertical pixel density
            (density.v >> 8) as u8,
            (density.v & 0xff) as u8,

            // thumbnail width (unsupported)
            0x00,

            // thumbnail height (unsupported)
            0x00,
        ];

        write_marker(w, &Marker::new(MarkerType::App0, &payload))?;

        Ok(JfifExtension)
    }
}

impl JfifExtension {
    // FIXME: decide if we support a generic write_marker(), or
    // if we actually define individual methods for comment, ICC profile,
    // thumbnail, etc.
    //
    // For now, we just provide next() to move to the next stage.

    pub fn next(self) -> Frame<Sample8> {
        // JFIF supports only 8-bit samples, that's why we return a Frame<Sample8>
        Frame {
            sample: PhantomData,
        }
    }
}

impl<T: Sample + Sized> Frame<T> {
    /// Writes quantization tables and a start-of-frame header.
    ///
    /// Each item in `quant_tables` is a tuple with a quantization table, and the table's
    /// identifier (its index in the 4-element array of quantization tables for an
    /// encoder).
    ///
    /// # Panics
    ///
    /// Will panic if any of the quantization table indices (the `usize` in the
    /// `quant_table` tuples) is larger than 3, as JPEG only supports up to 4 quantization
    /// tables.
    pub fn write<W: Write>(
        &self,
        quant_tables: &[(&QuantTable<T>, usize)],
        w: &mut W,
    ) -> io::Result<NextStateFIXME> {
        // emit_dqt

        for (_q, i) in quant_tables {
            assert!(*i < NUM_QUANT_TABLES);
        }

        for (q, i) in quant_tables {
            self.write_quant_table(*q, *i, w)?;
        }

        Ok(NextStateFIXME)
    }

    fn write_quant_table<W: Write>(
        &self,
        quant_table: &QuantTable<T>,
        index: usize,
        w: &mut W,
    ) -> io::Result<()> {
        let precision = <T as Sample>::QUANT_TABLE_PRECISION;

        // checked in write() above
        assert!(index < NUM_QUANT_TABLES);
        let index = u8(index).unwrap();

        let precision_and_index = (precision << 4) + index;

        // precision_and_index + quant_table_values
        let length = 1 + mem::size_of::<T>() * quant_table.values.len();

        let mut buf = vec![0u8; 1 + mem::size_of::<T>() * DCT_SIZE_SQUARED];
        buf[0] = precision_and_index;

        quant_table.serialize(&mut buf[1..]);

        write_marker(w, &Marker::new(MarkerType::DefineQuantizationTables, &buf))?;

        Ok(())
    }
}

fn write_marker_header<W: Write>(
    w: &mut W,
    type_: MarkerType,
    payload_len: usize,
) -> io::Result<()> {
    w.write_all(&[0xff, type_.code()])?;

    if type_.is_start_of_marker_segment() {
        let payload_len =
            u16(payload_len + 2).expect("Marker should already have checked its length");

        w.write_u16::<BigEndian>(payload_len)?;
    }

    Ok(())
}

fn write_marker<W: Write>(w: &mut W, marker: &Marker) -> io::Result<()> {
    write_marker_header(w, marker.type_, marker.payload.len())?;
    w.write_all(marker.payload)?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[rustfmt::skip]
    fn test_jfif_square_pixels() {
        let mut buf = Vec::<u8>::new();

        assert!(Jfif.write(
            &Density::new(Units::AspectRatio, 1, 1),
            &mut buf,
        ).is_ok());
        assert_eq!(
            buf,
            [0xff, MarkerType::App0.code(),
             0x00, 0x10,
             'J' as u8, 'F' as u8, 'I' as u8, 'F' as u8, 0x00,
             0x01, 0x01,
             0x00,
             0x00, 0x01,
             0x00, 0x01,
             0x00, 0x00]
        );
    }

    #[test]
    #[rustfmt::skip]
    fn test_jfif_rectangular_pixels() {
        let mut buf = Vec::<u8>::new();

        assert!(Jfif.write(
            &Density::new(Units::AspectRatio, 2, 1),
            &mut buf,
        ).is_ok());
        assert_eq!(
            buf,
            [0xff, MarkerType::App0.code(),
             0x00, 0x10,
             'J' as u8, 'F' as u8, 'I' as u8, 'F' as u8, 0x00,
             0x01, 0x01,
             0x00,
             0x00, 0x02,
             0x00, 0x01,
             0x00, 0x00]
        );
    }

    #[test]
    #[rustfmt::skip]
    fn test_jfif_ppi() {
        let mut buf = Vec::<u8>::new();

        assert!(Jfif.write(
            &Density::new(Units::PointsPerIn, 300, 600),
            &mut buf,
        ).is_ok());
        assert_eq!(
            buf,
            [0xff, MarkerType::App0.code(),
             0x00, 0x10,
             'J' as u8, 'F' as u8, 'I' as u8, 'F' as u8, 0x00,
             0x01, 0x01,
             0x01,
             0x01, 0x2c,
             0x02, 0x58,
             0x00, 0x00]
        );
    }

    #[test]
    #[rustfmt::skip]
    fn test_jfif_ppcm() {
        let mut buf = Vec::<u8>::new();

        assert!(Jfif.write(
            &Density::new(Units::PointsPerCm, 300, 600),
            &mut buf,
        ).is_ok());
        assert_eq!(
            buf,
            [0xff, MarkerType::App0.code(),
             0x00, 0x10,
             'J' as u8, 'F' as u8, 'I' as u8, 'F' as u8, 0x00,
             0x01, 0x01,
             0x02,
             0x01, 0x2c,
             0x02, 0x58,
             0x00, 0x00]
        );
    }
}
