/// Pixel units for the JFIF header.
///
/// See the [`Density`] struct for more information.
///
/// [`Density`]: struct.Density.html
pub enum Units {
    /// Dimensionless pixels with an `h:v` aspect ratio.
    AspectRatio,

    /// `h:v` are in points-per-inch.
    PointsPerIn,

    /// `h:v` are in points-per-centimeter.
    PointsPerCm,
}

/// Pixel density for the JFIF header.
///
/// The default has [`Units.AspectRatio`] with `h=v=1`, which means simply
/// "square pixels".
///
/// You can set other pixel ratios, or dimensions with respect to physical units,
/// by using other values of [`Units`] when calling the [`new`] method.
///
/// [`Units.AspectRatio`]: enum.Units.html#variant.AspectRatio
/// [`Units`]: enum.Units.html
/// [`new`]: #method.new
pub struct Density {
    pub units: Units,
    pub h: u16,
    pub v: u16,

    // So that callers have to go through the `new` constructor
    _uninstantiable: (),
}

/// Creates a value for "dimensionless square pixels", with [`Units.AspectRatio`] and `h=v=1`.
///
/// [`Units.AspectRatio`]: enum.Units.html#variant.AspectRatio
impl Default for Density {
    fn default() -> Density {
        Density {
            units: Units::AspectRatio,
            h: 1,
            v: 1,
            _uninstantiable: (),
        }
    }
}

impl Density {
    /// Creates a [`Density`] value.
    ///
    /// # Panics
    ///
    /// Will panic if `h` or `v` are zero.
    ///
    /// [`Density`]: struct.Density.html
    pub fn new(units: Units, h: u16, v: u16) -> Density {
        assert!(h > 0 && v > 0);
        Density {
            units,
            h,
            v,
            _uninstantiable: (),
        }
    }
}
