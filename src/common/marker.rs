/// A JPEG marker and its payload.
pub struct Marker<'a> {
    pub(crate) type_: MarkerType,
    pub(crate) payload: &'a [u8],
}

impl<'a> Marker<'a> {
    /// Creates a new marker, and validates its payload length.
    ///
    /// # Panics
    ///
    /// If `type_` indicates a standalone marker, that is, one that is not the start of a
    /// marker segment and thus has no payload, then the `payload.len()` must be zero.
    /// For example, `MarkerType::StartOfImage` and `MarkerType::EndOfImage` are
    /// standalone markers (i.e.  `type_.is_start_of_marker_segment()` returns `false` for
    /// them), and must have an empty payload.
    ///
    /// Also panics if a marker that allows payloads has a payload length larger than
    /// 65533.  This is 65535 - 2, because the length field for a marker is a 16-bit
    /// integer and includes the length of the length field itself.
    pub fn new(type_: MarkerType, payload: &[u8]) -> Marker {
        let max_length = type_.max_payload_length();
        assert!(
            payload.len() <= max_length,
            "maximum payload length for {:?} is {}",
            type_,
            max_length,
        );

        Marker { type_, payload }
    }
}

macro_rules! markers {
    {
        marker_type_docs { $(#[$marker_type_docs:meta])+ }
        code_docs { $(#[$code_docs:meta])+ }
        from_code_docs { $(#[$from_code_docs:meta])+ }

        $($name:ident : $code:expr,)+
    } => {
        $(#[$marker_type_docs])+
        #[derive(Copy, Clone, Debug, PartialEq)]
        pub enum MarkerType {
            $($name,)+

            Reserved(u8),
        }

        impl MarkerType {
            $(#[$code_docs])+
            pub fn code(&self) -> u8 {
                match *self {
                    $(MarkerType::$name => $code,)+

                    MarkerType::Reserved(code) => code,
                }
            }

            $(#[$from_code_docs])+
            pub fn from_code(code: u8) -> Result<MarkerType, ()> {
                match code {
                    $($code => Ok(MarkerType::$name),)+

                    0x02 ..= 0xbf => Ok(MarkerType::Reserved(code)),

                    _ => Err(())
                }
            }
        }
    };
}

impl MarkerType {
    pub fn max_payload_length(&self) -> usize {
        if self.is_start_of_marker_segment() {
            // The field for marker segment length is two bytes, and its value includes
            // the length of the field.  So, the maximum is the u16 range minus 2.
            65535 - 2
        } else {
            0
        }
    }
}

// CCITT Recc. T.81 (1992 E)
// Table B.1 - Marker Code Assignments
//
// This uses descriptive names to avoid scattering infrequently-used acronyms through
// the code.
//
// Note that the table in the spec has a slightly different ordering than the following.
// We reorder it by numerical value; the groupings are the same as in the spec.
#[rustfmt::skip]
markers! {
    marker_type_docs {
        /// All the valid JPEG marker types.
        ///
        /// Note that `0x00` and `0xff` are not valid marker types; these are
        /// rejected by [`MarkerType::from_code`].
        ///
        /// [`MarkerType::from_code`]: #method.from_code
    }

    code_docs {
        /// Returns the marker's code as a `u8`.
    }

    from_code_docs {
        /// Creates a marker type based on its numerical code.
        ///
        /// All possible values in a `u8` are valid marker types, except
        /// for 0x00 and 0xff.  For these last two, the function returns
        /// `Err(())`.
    }

    // Start of Frame markers, non-differential, Huffman coding

    StartOfFrameBaselineDCT:             0xc0, // SOF0
    StartOfFrameExtendedSequentialDCT:   0xc1, // SOF1
    StartOfFrameProgressiveDCT:          0xc2, // SOF2
    StartOfFrameLossless:                0xc3, // SOF3

    // Huffman table specification

    DefineHuffmanTables:                 0xc4, // DHT

    // Start of Frame markers, differential, Huffman coding

    StartOfFrameDiffSequentialDCT:       0xc5, // SOF5
    StartOfFrameDiffProgressiveDCT:      0xc6, // SOF6
    StartOfFrameDiffLossless:            0xc7, // SOF7

    // Start of Frame markers, non-differential, arithmetic coding

    StartOfFrameJpegExtensions:          0xc8, // JPG

    StartOfFrameExtSequentialDCTArith:   0xc9, // SOF9
    StartOfFrameProgressiveDCTArith:     0xca, // SOF10
    StartOfFrameLosslessArith:           0xcb, // SOF11

    // Arithmetic coding conditioning specification

    DefineArithmeticConditioning:        0xcc, // DAC

    // Start of Frame markers, differential, arithmetic coding

    StartOfFrameDiffSequentialDCTArith:  0xcd, // SOF13
    StartOfFrameDiffProgressiveDCTArith: 0xce, // SOF14
    StartOfFrameDiffLosslessArith:       0xcf, // SOF15

    // Restart interval termination

    Restart0:                            0xd0, // RST0
    Restart1:                            0xd1, // RST1
    Restart2:                            0xd2, // RST2
    Restart3:                            0xd3, // RST3
    Restart4:                            0xd4, // RST4
    Restart5:                            0xd5, // RST5
    Restart6:                            0xd6, // RST6
    Restart7:                            0xd7, // RST7

    // Other markers

    StartOfImage:                        0xd8, // SOI
    EndOfImage:                          0xd9, // EOI
    StartOfScan:                         0xda, // SOS
    DefineQuantizationTables:            0xdb, // DQT
    DefineNumberOfLines:                 0xdc, // DNL
    DefineRestartInterval:               0xdd, // DRI
    DefineHierarchicalProgression:       0xde, // DHP
    ExpandReferenceComponents:           0xdf, // EXP

    App0:                                0xe0, // APP0
    App1:                                0xe1, // APP1
    App2:                                0xe2, // APP2
    App3:                                0xe3, // APP3
    App4:                                0xe4, // APP4
    App5:                                0xe5, // APP5
    App6:                                0xe6, // APP6
    App7:                                0xe7, // APP7
    App8:                                0xe8, // APP8
    App9:                                0xe9, // APP9
    App10:                               0xea, // APP10
    App11:                               0xeb, // APP11
    App12:                               0xec, // APP12
    App13:                               0xed, // APP13
    App14:                               0xee, // APP14
    App15:                               0xef, // APP15

    JpegExtension0:                      0xf0, // JPG0
    JpegExtension1:                      0xf1, // JPG1
    JpegExtension2:                      0xf2, // JPG2
    JpegExtension3:                      0xf3, // JPG3
    JpegExtension4:                      0xf4, // JPG4
    JpegExtension5:                      0xf5, // JPG5
    JpegExtension6:                      0xf6, // JPG6
    JpegExtension7:                      0xf7, // JPG7
    JpegExtension8:                      0xf8, // JPG8
    JpegExtension9:                      0xf9, // JPG9
    JpegExtension10:                     0xfa, // JPG10
    JpegExtension11:                     0xfb, // JPG11
    JpegExtension12:                     0xfc, // JPG12
    JpegExtension13:                     0xfd, // JPG13

    Comment:                             0xfe, // COM

    // Reserved markers

    TempArithmetic:                      0x01, // TEM


}

impl MarkerType {
    /// If a marker is the start of a marker segment, it will have length+payload fields.
    ///
    /// Otherwise, it is standalone and is just 0xffzz, where zz is the marker's code.
    pub fn is_start_of_marker_segment(&self) -> bool {
        use MarkerType::*;

        match *self {
            StartOfImage | EndOfImage | TempArithmetic => false,

            Restart0 | Restart1 | Restart2 | Restart3 | Restart4 | Restart5 | Restart6
            | Restart7 => false,

            _ => true,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn handles_reserved_markers() {
        assert_eq!(MarkerType::from_code(0x02), Ok(MarkerType::Reserved(0x02)));
        assert_eq!(MarkerType::from_code(0x02).unwrap().code(), 0x02);

        assert_eq!(MarkerType::from_code(0xbf), Ok(MarkerType::Reserved(0xbf)));
        assert_eq!(MarkerType::from_code(0xbf).unwrap().code(), 0xbf);
    }

    #[test]
    fn standalone_markers() {
        assert!(!MarkerType::StartOfImage.is_start_of_marker_segment());
    }

    #[test]
    fn detects_invalid_markers() {
        assert!(MarkerType::from_code(0x00).is_err());
        assert!(MarkerType::from_code(0xff).is_err());
    }
}
