use std::f32;
use std::marker::PhantomData;
use std::ptr;

use itertools::izip;
use libc::c_long;
use mozjpeg_sys::{
    jnosimd_rgb_ycc_convert, jpeg_color_converter_input, jsimd_can_rgb_ycc, jsimd_rgb_ycc_convert,
    JCS_RGB,
};

use crate::common::types::{samples_in_padded_row, Sample, Sample8};

/// Packed RGB to planar YCbCr conversion.
///
/// We use this trait to abstract out the different implementations of the algorithm.
pub trait RgbToYcc {
    type Sample;

    /// Runs the RGB to YCbCr conversion.
    ///
    /// The `setup` value should have been created with the `setup()` method beforehand.
    fn rgb_to_ycc(
        &self,
        input: &[Self::Sample],
        out_y: &mut [Self::Sample],
        out_cb: &mut [Self::Sample],
        out_cr: &mut [Self::Sample],
    );
}

/// Lookup table for the RGB to YCbCr conversion done in Rust.
pub struct RgbYccRust<T: Sample> {
    table: Box<[i32]>,
    sample: PhantomData<T>,
}

// Builds a lookup table for RGB to YCbCr conversion.  The parameters
// come from the constants in `types::Sample`.
fn make_lookup_table(range_upper: usize, center: i32) -> Box<[i32]> {
    // copied from jccolor.c
    // Logic from rgb_ycc_start()

    let subtable_size = range_upper;
    let table_size = 8 * subtable_size;

    let one_half: i32 = 1 << (16 - 1);
    let cbcr_offset: i32 = center << 16;

    let mut table = vec![0; table_size];

    {
        // Split the table mutably into sub-tables
        let (r_y, table) = table.split_at_mut(subtable_size);
        let (g_y, table) = table.split_at_mut(subtable_size);
        let (b_y, table) = table.split_at_mut(subtable_size);

        let (r_cb, table) = table.split_at_mut(subtable_size);
        let (g_cb, table) = table.split_at_mut(subtable_size);
        let (b_cb, table) = table.split_at_mut(subtable_size);

        // B=>Cb and R=>Cr tables are the same
        // let (r_cr, table) = table.split_at_mut(subtable_size!());
        let (g_cr, table) = table.split_at_mut(subtable_size);
        let (b_cr, table) = table.split_at_mut(subtable_size);

        for i in 0..subtable_size {
            let ii = i as i32;

            r_y[i] = fix(0.29900) * ii;
            g_y[i] = fix(0.58700) * ii;
            b_y[i] = fix(0.11400) * ii + one_half;

            r_cb[i] = (-fix(0.16874)) * ii;
            g_cb[i] = (-fix(0.33126)) * ii;
            // We use a rounding fudge-factor of 0.5-epsilon for Cb and Cr.
            // This ensures that the maximum output will round to MAXJSAMPLE
            // not MAXJSAMPLE+1, and thus that we don't have to range-limit.
            b_cb[i] = fix(0.50000) * ii + cbcr_offset + one_half - 1;

            // B=>Cb and R=>Cr tables are the same
            // r_cr[i] = fix(0.50000) * ii + cbcr_offset + one_half - 1;

            g_cr[i] = (-fix(0.41869)) * ii;
            b_cr[i] = (-fix(0.08131)) * ii;
        }
    }

    table.into_boxed_slice()
}

impl<T: Sample> RgbYccRust<T> {
    pub fn new() -> Self {
        let range_upper = <T as Sample>::RANGE_UPPER;
        let center = <T as Sample>::CENTER;

        RgbYccRust {
            table: make_lookup_table(range_upper, center),
            sample: PhantomData,
        }
    }
}

impl<T: Sample> RgbToYcc for RgbYccRust<T> {
    type Sample = T;

    /// Converts a packed RGB scanline into three scanlines for individual Y/Cb/Cr planes.
    ///
    /// # Panics:
    ///
    /// Will panic if the following conditions are not met:
    ///
    /// * The `out_y`, `out_cb`, `out_cr` slices must have the same length.
    ///
    /// * The length of `input` must be 3 times as large as the length
    /// of each of the output slices.
    fn rgb_to_ycc(&self, input: &[T], out_y: &mut [T], out_cb: &mut [T], out_cr: &mut [T]) {
        // Logic from jccolext.c:rgb_ycc_convert_internal()
        // That one processes several scanlines; this function does a single one.

        // FIXME: the original function gets #include'd with different #defines
        // for the RGB_RED/RGB_GREEN/RB_BLUE indices, and so it is used to generate
        // code for RGB, BGR, etc.  Decide if we want to support non-RGB inputs.

        assert!(input.len() == out_y.len() * 3);
        assert!(input.len() == out_cb.len() * 3);
        assert!(input.len() == out_cr.len() * 3);

        let subtable_size = <T as Sample>::RANGE_UPPER;
        let num_cols = out_y.len();

        let table = &self.table[..];

        // Split the table into sub-tables
        let (r_y, table) = table.split_at(subtable_size);
        let (g_y, table) = table.split_at(subtable_size);
        let (b_y, table) = table.split_at(subtable_size);

        let (r_cb, table) = table.split_at(subtable_size);
        let (g_cb, table) = table.split_at(subtable_size);
        let (b_cb, table) = table.split_at(subtable_size);

        // B=>Cb and R=>Cr tables are the same
        let r_cr = b_cb;
        let (g_cr, table) = table.split_at(subtable_size);
        let (b_cr, table) = table.split_at(subtable_size);

        for (rgb, y, cb, cr) in izip!(input.chunks_exact(3), out_y, out_cb, out_cr) {
            let r = rgb[0].get() as usize;
            let g = rgb[1].get() as usize;
            let b = rgb[2].get() as usize;

            // If the inputs are 0..MAXJSAMPLE, the outputs of these equations
            // must be too; we do not need an explicit range-limiting operation.

            *y = <T as Sample>::from((r_y[r] + g_y[g] + b_y[b]) >> 16);
            *cb = <T as Sample>::from((r_cb[r] + g_cb[g] + b_cb[b]) >> 16);
            *cr = <T as Sample>::from((r_cr[r] + g_cr[g] + b_cr[b]) >> 16);
        }
    }
}

fn fix(x: f32) -> i32 {
    // FIXME: use cast crate to avoid "as i32"
    (x * 65536.0 + 0.5) as i32
}

/// RGB to YCbCr conversion done in C by libjpeg-turbo.
pub struct RgbYccNoSimd {
    table: Box<[c_long]>,
}

impl RgbYccNoSimd {
    pub fn new() -> Self {
        // While make_lookup_table generates an [i32] table, libjpeg-turbo uses a [JLONG] one.
        // It says that JLONG should be at least a 32-bit integer, but doesn't seem to
        // mind that it is longer on 64-bit platforms.
        //
        // So, this function converts our [i32] table into [JLONG].
        let table = make_lookup_table(Sample8::RANGE_UPPER, Sample8::CENTER);

        RgbYccNoSimd {
            table: table.iter().map(|v| *v as c_long).collect(),
        }
    }
}

impl RgbToYcc for RgbYccNoSimd {
    type Sample = u8;

    fn rgb_to_ycc(&self, input: &[u8], out_y: &mut [u8], out_cb: &mut [u8], out_cr: &mut [u8]) {
        assert!(input.len() == out_y.len() * 3);
        assert!(input.len() == out_cb.len() * 3);
        assert!(input.len() == out_cr.len() * 3);

        let table = &self.table[..];

        let input_samparray = [input.as_ptr()];

        let mut plane_y = [out_y.as_mut_ptr()];
        let mut plane_cb = [out_cb.as_mut_ptr()];
        let mut plane_cr = [out_cr.as_mut_ptr()];

        let mut output = [
            plane_y.as_mut_ptr(),
            plane_cb.as_mut_ptr(),
            plane_cr.as_mut_ptr(),
        ];

        let color_converter_input = jpeg_color_converter_input {
            input_buf: input_samparray.as_ptr(),
            image_width: out_y.len() as u32,
            in_color_space: JCS_RGB,
            input_components: 3,
            num_components: 3,
            rgb_ycc_tab: table.as_ptr(),
        };

        // This is only a bit suspicious in that we are generating the rgb_ycc_tab with
        // our own code instead of the original C code from libjpeg-turbo.  Ideally we'd
        // patch that library to allow calling the function that generates the table.  But
        // what could possibly go wrong by trusting our port to Rust?
        unsafe {
            jnosimd_rgb_ycc_convert(&color_converter_input, output.as_mut_ptr(), 0, 1);
        }
    }
}

/// RGB to YCbCr conversion done by libjpeg-turbo's SIMD functions.
pub struct RgbYccSimd;

impl RgbYccSimd {
    /// Returns `Err(())` if libjpeg-turbo cannot do RGB to YCbCr conversion with SIMD code.
    pub fn new() -> Result<Self, ()> {
        unsafe {
            if jsimd_can_rgb_ycc() != 0 {
                Ok(RgbYccSimd)
            } else {
                Err(())
            }
        }
    }
}

impl RgbToYcc for RgbYccSimd {
    type Sample = u8;

    fn rgb_to_ycc(&self, input: &[u8], out_y: &mut [u8], out_cb: &mut [u8], out_cr: &mut [u8]) {
        assert!(input.len() % 3 == 0);
        let num_samples = input.len() / 3;
        let num_padded_samples = samples_in_padded_row::<Sample8>(num_samples);

        assert!(out_y.len() == num_padded_samples);
        assert!(out_cb.len() == num_padded_samples);
        assert!(out_cr.len() == num_padded_samples);

        let input_samparray = [input.as_ptr()];

        let mut plane_y = [out_y.as_mut_ptr()];
        let mut plane_cb = [out_cb.as_mut_ptr()];
        let mut plane_cr = [out_cr.as_mut_ptr()];

        let mut output = [
            plane_y.as_mut_ptr(),
            plane_cb.as_mut_ptr(),
            plane_cr.as_mut_ptr(),
        ];

        let input = jpeg_color_converter_input {
            input_buf: input_samparray.as_ptr(),
            image_width: num_samples as u32,
            in_color_space: JCS_RGB,
            input_components: 3,
            num_components: 3,
            rgb_ycc_tab: ptr::null(),
        };

        unsafe {
            jsimd_rgb_ycc_convert(&input, output.as_mut_ptr(), 0, 1);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use itertools;

    use crate::common::types::{Sample, Sample8};

    #[test]
    fn rgb_ycc_computes_same_as_original() {
        let mut source = [0u8; 256 * 3 * 3];

        for i in 0..256 {
            source[0 * 256 + i * 3 + 0] = i as u8;
            source[0 * 256 + i * 3 + 1] = 0;
            source[0 * 256 + i * 3 + 2] = 0;
        }

        for i in 0..256 {
            source[1 * 256 + i * 3 + 0] = 0;
            source[1 * 256 + i * 3 + 1] = i as u8;
            source[1 * 256 + i * 3 + 2] = 0;
        }

        for i in 0..256 {
            source[2 * 256 + i * 3 + 0] = 0;
            source[2 * 256 + i * 3 + 1] = 0;
            source[2 * 256 + i * 3 + 2] = i as u8;
        }

        let mut expected_y = [0u8; 256 * 3];
        let mut expected_cb = [0u8; 256 * 3];
        let mut expected_cr = [0u8; 256 * 3];

        let mut actual_y = [0u8; 256 * 3];
        let mut actual_cb = [0u8; 256 * 3];
        let mut actual_cr = [0u8; 256 * 3];

        RgbYccNoSimd::new().rgb_to_ycc(
            &source,
            &mut expected_y,
            &mut expected_cb,
            &mut expected_cr,
        );
        RgbYccRust::new().rgb_to_ycc(&source, &mut actual_y, &mut actual_cb, &mut actual_cr);

        itertools::assert_equal(expected_y.iter(), actual_y.iter());
        itertools::assert_equal(expected_cb.iter(), actual_cb.iter());
        itertools::assert_equal(expected_cr.iter(), actual_cr.iter());
    }
}
